#!/bin/bash
function error()
{
    echo "$*" >&2
    exit 1
}

while [ `whoami` != root ]; do
	error "Please run as root"
done
## FQDN & IP
read -p "Enter FQDN: " fqdn
echo $fqdn > /etc/hostname
hostname -F /etc/hostname
ip=$(ifconfig eth0 | perl -nle'/dr:(\S+)/ && print $1')
echo "Detected IP (eth0):" $ip
while [[ $REPLY != n || $REPLY != N ]]; do
	read -p "Is this correct? (y/n)"
	[ $REPLY = n -o $REPLY = N ] && read -p "Enter IP Address: " ip && break || break
done
echo "$ip	$fqdn" >> /etc/hosts

## ADD USER
read -p "Enter Username: " username
adduser $username
usermod -a -G sudo $username

## SSH
read -p "Enter your SSH Public Key: " sshkey
mkdir -p /home/$username/.ssh/
echo $sshkey >> /home/$username/.ssh/authorized_keys
perl -pi -e 's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
perl -pi -e 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config

mv /etc/apt/sources.list /etc/apt/sources.list.old
echo "deb mirror://mirrors.ubuntu.com/mirrors.txt precise main restricted universe multiverse
deb mirror://mirrors.ubuntu.com/mirrors.txt precise-updates main restricted universe multiverse
deb mirror://mirrors.ubuntu.com/mirrors.txt precise-backports main restricted universe multiverse
deb mirror://mirrors.ubuntu.com/mirrors.txt precise-security main restricted universe multiverse" > /etc/apt/sources.list

## UPDATE SYSTEM
apt-get update && apt-get upgrade -y

## INSTALL PACKAGES
apt-get install -y fail2ban logrotate nano bash-completion

## RESTART SERVICES
service ssh restart