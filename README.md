#Ubuntu - Initial Setup Script


Licensed Under the [WTFPL](http://www.wtfpl.net/)

Author: Anand Kapre

Version: 1.0

1. Sets the Fully Qualified Domain Name (FQDN) of the Server.
2. Creates a new user with sudo access.
3. Adds a SSH Public Key for that user.
4. Secures the SSH server by preventing root login and also disabling password authentication.
5. Updates sources to mirror:// links and Upgrades the server.
6. Restarts SSH. 